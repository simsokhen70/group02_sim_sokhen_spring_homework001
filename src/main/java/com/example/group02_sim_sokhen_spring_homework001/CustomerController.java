package com.example.group02_sim_sokhen_spring_homework001;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
@RequestMapping("api/v1")
public class CustomerController {
    ArrayList<Customer> customers = new ArrayList<>();
    static int currentId = 4;

    // Insert 3 default customer data
    public CustomerController(){
        customers.add(new Customer( 1,"Kao SeavPinh", "Male", 21, "PP"));
        customers.add(new Customer( 2,"Srey SiTharo", "Male", 21, "KPS"));
        customers.add(new Customer( 3,"Vorn Naro", "Male", 21, "PVH"));
    }

    @GetMapping ("/customers")
    public ApiResponsee getAllCustomer() {
        ArrayList<Customer> customerList = new ArrayList<>();
        for (Customer customer : customers) {
            Customer customers = new Customer(customer.getId(), customer.getName(), customer.getGender(), customer.getAge(), customer.getAddress());
            customerList.add(customers);
        }
        return new ApiResponsee("You got all customer successfully", customerList, "OK", LocalDateTime.now());
    }

    // Insert customer data
//    @PostMapping("/customers")
//    public ResponseEntity<ApiResponsee> addCustomer(@RequestBody Customer customer) {
//        customer.setId(currentId++);
//        customers.add(customer);
//
//        ApiResponsee response = new ApiResponsee("This record was successfully created", customer, "OK", LocalDateTime.now());
//
//        return ResponseEntity.status(HttpStatus.CREATED).body(response);
//    }
    @PostMapping("/customers")
    public ResponseEntity<ApiResponsee> addCustomer(@RequestBody NewCustomer newCustomerRequest) {
        Customer customer = new Customer();
        customer.setName(newCustomerRequest.getName());
        customer.setGender(newCustomerRequest.getGender());
        customer.setAge(newCustomerRequest.getAge());
        customer.setAddress(newCustomerRequest.getAddress());

        customer.setId(currentId++);
        customers.add(customer);

        ApiResponsee response = new ApiResponsee("This record was successfully created", customer, "OK", LocalDateTime.now());

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }
    // Update customer
    @PutMapping("/customers/{customer_id}")
    public ResponseEntity<ApiResponsee<Customer>> updateCustomerById(@PathVariable("customer_id") Integer c_id, @RequestBody NewCustomer request) {
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getId() == c_id) {
                // update the customer with new data
                Customer updatedCustomer = customers.get(i);
                updatedCustomer.setName(request.getName());
                updatedCustomer.setGender(request.getGender());
                updatedCustomer.setAge(request.getAge());
                updatedCustomer.setAddress(request.getAddress());
                // create a response with updated customer data
                ApiResponsee<Customer> response = new ApiResponsee<>("You're update successfully", updatedCustomer, "OK", LocalDateTime.now());
                // return the response with HTTP status 200 OK
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }
        // return a response with HTTP status 404 Not Found if customer is not found
        ApiResponsee<Customer> response = new ApiResponsee<>("Customer not found", null, "Not Found", LocalDateTime.now());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
    @DeleteMapping("/customers/{customer_id}")
    public ApiResponse1 deleteCustomerById(@PathVariable("customer_id") Integer c_id) {
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getId() == c_id) {
                customers.remove(i);
                return new ApiResponse1("Congratulation your delete is successfully", "OK" , LocalDateTime.now());
            }
        }
        return new ApiResponse1("Sorry, the customer with ID " + c_id + " does not exist", "Not found" , LocalDateTime.now());
    }
    // Get customer by id
    @GetMapping("/customers/{customer_id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("customer_id") Integer c_id){
        for (Customer cus : customers){
            if(cus.getId() == c_id) {
                return ResponseEntity.ok(new ApiResponsee<>("This record has found successfully", cus, "OK", LocalDateTime.now()));
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponsee<>("No record found with ID " + c_id, null, "NOT_FOUND", LocalDateTime.now()));
    }

    // Get customer by name
    @GetMapping("/customer/search")
    public ResponseEntity<Object> searchCustomer (@RequestParam("customer_name") String name) {
        for (Customer cus : customers){
            if (cus.getName().equals(name)){
                return ResponseEntity.ok(new ApiResponsee<>("This record has found successfully", cus, "OK", LocalDateTime.now()));
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiResponsee<>("No record found with name " + name, null, "NOT_FOUND", LocalDateTime.now()));
    }

}
