package com.example.group02_sim_sokhen_spring_homework001;

import java.time.LocalDateTime;

public class ApiResponse1<T> {

    private String message;
    private String status;
    private LocalDateTime time;

    // Constructor
    public ApiResponse1(String message, String status, LocalDateTime time) {
        this.message = message;
        this.status = status;
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}
