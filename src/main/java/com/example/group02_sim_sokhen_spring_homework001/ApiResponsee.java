package com.example.group02_sim_sokhen_spring_homework001;

import java.time.LocalDateTime;

public class ApiResponsee<T> {

    private String message;
    private T customer_info;
    private String status;
    private LocalDateTime time;

    // Constructor
    public ApiResponsee(String message, T customer_info, String status, LocalDateTime time) {
        this.message = message;
        this.customer_info = customer_info;
        this.status = status;
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomer_info() {
        return customer_info;
    }

    public void setCustomer_info(T customer_info) {
        this.customer_info = customer_info;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}





