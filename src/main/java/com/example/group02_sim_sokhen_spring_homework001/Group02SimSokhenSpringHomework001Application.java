package com.example.group02_sim_sokhen_spring_homework001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Group02SimSokhenSpringHomework001Application {

    public static void main(String[] args) {
        SpringApplication.run(Group02SimSokhenSpringHomework001Application.class, args);
    }

}
